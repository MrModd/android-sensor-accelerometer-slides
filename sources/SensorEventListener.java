public class SensorActivity extends Activity implements SensorEventListener {
private SensorManager mSensorManager;
private Sensor sensor;

@Override
public final void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main);

	mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
	mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
}

@Override
public final void onAccuracyChanged(Sensor sensor, int accuracy) {
	//Metodo richiamato quando cambia l'accuratezza del sensore
}

@Override
public final void onSensorChanged(SensorEvent event) {
	//Metodo richiamato quando viene generato un nuovo evento
}
