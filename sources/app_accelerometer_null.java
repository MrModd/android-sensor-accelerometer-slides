seekPrecision.setProgress( 5*(int)(6-SENSITIVITY_X) );

sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		
accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
if (accelerometer != null) {
	fieldName.setText(String.format("%s %s",accelerometer.getVendor(),accelerometer.getName()));
	buttonStart.setEnabled(true);
}

gravity = new float[3];
linear_acceleration = new float[3];

slow_average = WMA.getArrayOf(3, slow_beta, slow_beta);
fast_average = WMA.getArrayOf(3, fast_beta, fast_beta);