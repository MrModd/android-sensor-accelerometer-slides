public class WMA {
	float mean;
	float variance;
	float alpha;
	float beta;
	
	public static WMA[] getArrayOf(int n, float alpha, float beta) {
		WMA[] objects = new WMA[n];
		
		for (int i=0; i < n; i++)
			objects[i] = new WMA(alpha, beta);
		
		return objects;
	}
	
	public WMA(float alpha, float beta) {
		this.alpha = alpha;
		this.beta = beta;
		this.mean = 0;
		this.variance = 0;
	}	
	...
}
