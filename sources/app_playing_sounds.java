if (fast_average[0].getMean() - X_mean < -SENSITIVITY_X) {
	// Toast.makeText(this, getString(R.string.right), Toast.LENGTH_SHORT).show();
	playSnareSix.start();
}
else if (fast_average[0].getMean() - X_mean > SENSITIVITY_X) {
	// Toast.makeText(this, getString(R.string.left), Toast.LENGTH_SHORT).show();
	playSnareEight.start();
}

if (fast_average[2].getMean() - Z_mean < -SENSITIVITY_Z) {
	// Toast.makeText(this, getString(R.string.up), Toast.LENGTH_SHORT).show();
	playCrash.start();
}
else if (fast_average[2].getMean() - Z_mean > SENSITIVITY_Z) {
	// Toast.makeText(this, getString(R.string.down), Toast.LENGTH_SHORT).show();
	playKick.start();
}
