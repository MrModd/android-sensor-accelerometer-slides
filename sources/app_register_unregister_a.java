public void onButtonStartClicked(View v) {
    sensorManager.registerListener(this, accelerometer, sensorDelay);
		
    Toast.makeText(this, R.string.listner_reg, Toast.LENGTH_SHORT).show();

    buttonStart.setEnabled(false);
    buttonStop.setEnabled(true);
    buttonToggle.setEnabled(true);
}

public void onButtonStopClicked(View v) {
    sensorManager.unregisterListener(this);

    Toast.makeText(this, R.string.listner_unreg, Toast.LENGTH_SHORT).show();

    buttonStop.setEnabled(false);
    buttonStart.setEnabled(true);
    buttonToggle.setEnabled(false);
}