@Override
protected void onResume() {
    super.onResume();
    if (accelerometer != null) {
		buttonStart.setEnabled(true);
		buttonStop.setEnabled(false);
        buttonToggle.setEnabled(false);
    }
    
    ...
}
@Override
protected void onPause() {
    super.onPause();
	//Unregister from new accelerometer events when the activity goes away
    //from the screen
    //Note that unregistering an unregistered listener doesn't cause any error
    sensorManager.unregisterListener(this);
		
    if (buttonStop.isEnabled())
        //If the listener was actually registered show a message
        Toast.makeText(this, R.string.listner_unreg, Toast.LENGTH_SHORT).show();

    ...
}
