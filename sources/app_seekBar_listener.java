seekPrecision.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		SENSITIVITY_X = SENSITIVITY_Y = SENSITIVITY_Z = 6-0.2f*seekBar.getProgress();
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		float value = 6-0.2f*seekBar.getProgress();
		fieldThreshold.setText(String.format("%2.2f", value));
	}
});
