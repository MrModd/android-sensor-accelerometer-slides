@Override
public void onAccuracyChanged(Sensor sensor, int accuracy) {
}
@Override
public void onSensorChanged(SensorEvent event) {
	
	final float alpha = 0.8f;
	final float SENSITIVITY_X = 3.5f;
	final float SENSITIVITY_Z = 4.0f;
	final float SENSITIVITY_TILT = 55.0f;

	// Isolate the force of gravity with the low-pass filter.
	gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
	gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
	gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

	// Remove the gravity contribution with the high-pass filter.
	linear_acceleration[0] = event.values[0] - gravity[0];
	linear_acceleration[1] = event.values[1] - gravity[1];
	linear_acceleration[2] = event.values[2] - gravity[2];

	float X_mean = average[0].add(linear_acceleration[0]);
	float Y_mean = average[1].add(linear_acceleration[1]);
	float Z_mean = average[2].add(linear_acceleration[2]);

	float X_accel = linear_acceleration[0] - X_mean;
	float Y_accel = linear_acceleration[1] - Y_mean;
	float Z_accel = linear_acceleration[2] - Z_mean;

	float X_var = variance[0].add((float) Math.pow(X_accel, 2));
	float Y_var = variance[1].add((float) Math.pow(Y_accel, 2));
	float Z_var = variance[2].add((float) Math.pow(Z_accel, 2));


	fieldX.setText(String.valueOf(X_mean));
	fieldY.setText(String.valueOf(Y_mean));
	fieldZ.setText(String.valueOf(Z_mean));

	/*
	 * Take some action on acceleration bases, for each axes:
	 *
	 * 1) Mean + Threshold will detect the movement
	 * 2) Variance will be used to filter events frequency
	 *
	 */
	if ( X_var > SENSITIVITY_TILT ) {
		tilt = true;
	}
	if (!tilt) {
		if (X_accel < -SENSITIVITY_X && X_var < SENSITIVITY_X) {
			Toast.makeText(this, getString(R.string.right), Toast.LENGTH_SHORT).show();
		}
		else if (X_accel > SENSITIVITY_X && X_var < SENSITIVITY_X) {
			Toast.makeText(this, getString(R.string.left), Toast.LENGTH_SHORT).show();
		}
		if (Z_accel < -SENSITIVITY_Z && Z_var < SENSITIVITY_Z) {
			Toast.makeText(this, getString(R.string.up), Toast.LENGTH_SHORT).show();
		}
		else if (Z_accel > SENSITIVITY_Z && Z_var < SENSITIVITY_Z) {
			Toast.makeText(this, getString(R.string.down), Toast.LENGTH_SHORT).show();
		}
	}
	else {
		Toast.makeText(this, getString(R.string.tilt), Toast.LENGTH_SHORT).show();
			if ( X_var < 1) {
			tilt = false;
		}
	}
}