...

for (int i=0; i<3; i++) {
	slow_average[i].add(linear_acceleration[i]);
	fast_average[i].add(linear_acceleration[i]);
}
		
float X_mean = slow_average[0].getMean();
float Y_mean = slow_average[1].getMean();
float Z_mean = slow_average[2].getMean();
		
float X_var = slow_average[0].getVariance();
float Y_var = slow_average[1].getVariance();
float Z_var = slow_average[2].getVariance();

...