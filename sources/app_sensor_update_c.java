if (buttonToggle.isChecked()) {
	fieldX.setText(String.format("%2.4f", -X_mean));
	fieldY.setText(String.format("%2.4f", -Y_mean));
	fieldZ.setText(String.format("%2.4f", -Z_mean));
	varX.setText(String.format("%2.4f", X_var));
	varY.setText(String.format("%2.4f", Y_var));
	varZ.setText(String.format("%2.4f", Z_var));
}
else {
	fieldX.setText(String.format("%2.4f", -linear_acceleration[0]));
	fieldY.setText(String.format("%2.4f", -linear_acceleration[1]));
	fieldZ.setText(String.format("%2.4f", -linear_acceleration[2]));
	varX.setText(String.format("%2.4f", X_var));
	varY.setText(String.format("%2.4f", Y_var));
	varZ.setText(String.format("%2.4f", Z_var));
}