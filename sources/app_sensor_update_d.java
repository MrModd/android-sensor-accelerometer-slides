...
	
float X_fast_var = fast_average[0].getVariance();
float Y_fast_var = fast_average[1].getVariance();
float Z_fast_var = fast_average[2].getVariance();
		
X_fast_var *= X_fast_var;
Y_fast_var *= Y_fast_var;
Z_fast_var *= Z_fast_var;

if (fast_average[0].getMean() - X_mean < -SENSITIVITY_X) {
	playSnareSix.start();
}
else if (fast_average[0].getMean() - X_mean > SENSITIVITY_X) {
	playSnareEight.start();
}

if (fast_average[2].getMean() - Z_mean < -SENSITIVITY_Z) {
	playCrash.start();
}
else if (fast_average[2].getMean() - Z_mean > SENSITIVITY_Z) {
	playKick.start();
}

...
