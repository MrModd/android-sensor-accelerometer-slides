...

	public void add(float x) {
		//Update variance
		//This formula is the Exponential Moving Average applied for the variance.
		//beta has the same meaning of the alpha for the mean value (check below).
		variance = beta*variance + beta*(1-beta)*(x - mean)*(x - mean);
		
		//Update mean value
		//alpha determine how much old samples weight on the mean
		//the greater alpha is the more important the history is
		mean = alpha*mean + (1-alpha)*x;
	}
}
