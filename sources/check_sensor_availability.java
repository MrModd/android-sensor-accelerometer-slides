private SensorManager mSensorManager;
...
mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null){
	// Il dispositivo ha un magnetometro
}
else {
	// Non sono presenti sensori magnetici sul dispositivo corrente
}
